package ictgradschool.industry.assignment02.farmmanager.animals;

public class Kangaroo extends Animal implements IProductionAnimal {
    private final int MAX_VALUE = 500;

    public Kangaroo() {
        value = 300;
    }

    @Override
    public void feed() {
        if (value < MAX_VALUE) {
            value = value + (MAX_VALUE - value) / 2;

        }
    }

    @Override
    public int costToFeed() {
        return 2;
    }

    @Override
    public String getType() {
        return "Kangaroo";
    }
    public String toString() {
        return getType() + " - $" + value;
    }

    @Override
    public boolean harvestable() {
        return false;
    }

    @Override
    public int harvest() {
        return 0;
    }
}
